### Nginx Playbook  

#### How to run  
if you want to setup nginx with also serve static html for testing purpose:  
```ansible-playbook -i (inventory file) playbook.yml --extra-vars "deploy_static=true" ```

otherwise, just remove the `--extra-vars "deploy_static=true"`